package.path = "../?.lua;" .. package.path
local spc = require( 'spc_module' )

local function seq_to_string(seq)
   local str = '{'
   for i, v in ipairs(seq) do
      if (i == 1) then
	 str = str .. tostring(v)
      else
	 str = str .. ', ' .. tostring(v)
      end
   end
   str = str .. '}'
   return str
end

local function rule_test(test_name, func, val_res_array)
   print( 'Running test ' .. test_name .. '.')
   local all_tests_passed = true
   for i, test in ipairs( val_res_array ) do
      local argument = test[1]
      local expected_result = test[2]
      local result = func( argument )
      if (expected_result == result) then
	 print('  '..tostring(i)..': Passed : '..seq_to_string(argument)..' correctly returned '..tostring(result)..'.' )
      else
	 print('  '..tostring(i)..': FAILED : '..seq_to_string(argument)..' was expected to return '..tostring(expected_result)..' but returned '..tostring(result)..'.' )
	 all_tests_passed = false
      end
   end
   if all_tests_passed then
      print( ' All tests in ' .. test_name .. ' PASSED.' )
   else
      print( ' ' .. test_name .. ' FAILED.' )
   end
   return all_tests_passed
end




rule_test( 'RULE-1', spc.rule_1, {
	     {{}, false},
	     {{0}, false},
	     {{3}, true},
	     {{-3}, true},
	     {{2.99}, false},
	     {{0, 0, 0}, false},
	     {{0, 4, -10}, false},
	     {{4, 0, 0}, true}} )
      
rule_test( 'RULE-2', spc.rule_2, {
	     {{}, false},
	     {{0}, false},
	     {{1, 1, 1, 1, 1, 1, 1, 1}, false},
	     {{1, 1, 1, 1, 1, 1, 1, 1, 1}, true},
	     {{-1, -1, -1, -1, -1, -1, -1, -1, -1}, true},
	     {{1, 1, 1, 1, 1, 1, 1, 1, 1, -1}, true},
	     {{1, 1, 1, 1, 1, 1, 1, 1, -1}, false},
	     {{1, 1, 4, 1, 2, 1, 3, 1, 1}, true},
	     {{1, 1, 1, 1, 1, -1, 1, 1, 1}, false},} )   

rule_test( 'RULE-3', spc.rule_3, {
	     {{}, false},
	     {{0}, false},
	     {{1.1, 1.2, 1.3, 1.4, 1.5}, false},
	     {{1.1, 1.2, 1.3, 1.4, 1.5, 1.6}, true},
	     {{1.6, 1.5, 1.4, 1.3, 1.2, 1.1}, true},
	     {{1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7}, true},
	     {{1.6, 1.5, 1.4, 1.3, 1.2, 1.1, 1.0}, true},
	     {{1.1, 1.2, 1.3, 1.4, 1.5, 1.5, 1.5}, true},
	     {{1.6, 1.5, 1.4, 1.3, 1.2, 1.3, 1.2}, false},
	     {{1, 1, 1, 1, 1, 1}, true},
	     {{1.6, 1.5, 1.4, 1.5, 1.2, 1.1}, false},
	     {{-1.1, -1.2, -1.3, -1.4, -1.5, -1.6}, true},
	     {{-1.6, -1.5, -1.4, -1.3, -1.2, -1.1}, true},
} )   

rule_test( 'RULE-4', spc.rule_4, {
	      {{}, false},
	      {{0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0}, false},
	      {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, true},
	      {{0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 2, 1}, false},
	      {{0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, -1}, false},
	      {{0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1}, true},
	      {{1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0}, true},
	      {{1, 0, 1, 0, 1, 0, 1, 0, 1, 0, -0.1, 0, 1, 0}, false},
	      {{1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, -1}, true},
	      {{0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 2}, true},
} )

rule_test( 'RULE-5', spc.rule_5, {
	     {{}, false},
	     {{2, 1, 2}, true},
	     {{2, 2, 2}, true},
         {{-2, -2, -1}, true},
	     {{2, -2, -2}, false},
	     {{2, 1, -2}, false},
	     {{2, 2}, false},
	     {{-3, 0, -2}, true},
	     {{0, 3, 1, 2}, false},
} )

rule_test( 'RULE-6', spc.rule_6, {
	     {{}, false},
	     {{0, 2, 3, 2, 2}, false},
         {{2, 0, 3, 2, 2}, true},
	     {{0, 2, 2, 2, -2}, false},
         {{2, 0, 2, 2, -2}, false},
	     {{2, 2, 2, 2}, false},
	     {{2, 2, 2, 2, 0}, true},
	     {{-2, -2, 1, -2, -2}, true},
} )

rule_test( 'RULE-7', spc.rule_7, {
	      {{}, false},
	      {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, true},
	      {{0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0}, false},
	      {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, false},
	      {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, true},
} )

rule_test( 'RULE-8', spc.rule_8, {
	     {{}, false},
	     {{1, 1, 1, 1, 1, 1, 1, 1}, true},
	     {{1, 1, 1, 1, 0, 1, 1, 1}, false},
	     {{1, 1.5, 1, -1, 3.5, -2.3, 1, -1.3}, true},
	     } )  



-- Testing of the data stream memory functionality

function test_that(desc, test)
   if test then
      print("Passed " .. desc)
   else
      print("FAILED "..desc)
   end
end


function memory_equals(spc_obj, mem)
   local spc_mem = spc_obj.memory
   if #mem == #spc_mem then
      for i, v in ipairs(mem) do
	 if spc_mem[i].value ~= v then
	    return false
	 end
      end
      return true
   else
      return false
   end	 
end


-- Make sure we're building the state correctly.
spc.assert_valid_spc_state(spc.setup("","",""));

-- Test simple datum processing.
local spc_state = spc.process_datum(spc.setup("","",""),1, 0)
spc.assert_valid_spc_state(spc_state)
test_that( "stores 2,1", memory_equals(spc_state, {1}))

local spc_state = spc.process_datum(spc.process_datum(spc.setup("","",""),1, 0),2, 1)
test_that( "stores 2,1", memory_equals(spc_state, {2,1}))

test_that( "ignores nil data", memory_equals(spc.process_datum(spc_state, nil, 0), {2,1}) )
test_that( "ignores non-numeric data", memory_equals(spc.process_datum(spc_state, {1.0}, 0), {2,1}) )

spc_state = spc.setup("","","")
for i = 1, 40 do
   spc_state = spc.process_datum(spc_state, i, i)
end
test_that( "stores 30 of 40", memory_equals(spc_state, {40,39,38,37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11}))






-- Testing of the stats engine...

function eps_equal(n1, n2, eps)
   return math.abs(n1-n2) < eps;
end

function test_eps(desc, n1, n2, eps)
   if (math.abs(n1-n2) < eps) then
      print("Passed " .. desc .. ": value "..tostring(n1).." agrees with "..tostring(n2).." within "..tostring(eps)..".")
   else
      print("FAILED "..desc .. ": value "..tostring(n1).." falls outside "..tostring(n2).." by more than "..tostring(eps)..".")
   end
end

-- Stats test for constant sequence
spc_state = spc.setup("","","")
for i = 1, 100 do
   spc_state = spc.process_datum(spc_state, 10, i)
end
test_eps( "average of 100 10s", spc.stats_avg(spc_state), 10, 0.001)
-- print(spc.stats_avg(spc_state))
test_eps( "std-dev of 100 10s", spc.stats_std_dev(spc_state), 0, 0.001)
-- print(spc.stats_std_dev(spc_state))
-- print(spc_state.stats.var_a, spc_state.stats.var_b, spc_state.stats.var_c, spc_state.stats.count)


-- Stats test for linear sequence
spc_state = spc.setup("","","")
for i = 1, 100 do
   spc_state = spc.process_datum(spc_state, i, i)
end
test_eps( "average of 1-100", spc.stats_avg(spc_state), 50.5, 0.001)
test_eps( "std-dev of 1-100", spc.stats_std_dev(spc_state), 29.011, 0.001)

-- Stats test for long constant sequence
spc_state = spc.setup("","","")
for i = 1, 5000 do
   spc_state = spc.process_datum(spc_state, 10, i)
end
test_eps( "average of 5000 10s", spc.stats_avg(spc_state), 10, 0.001)
test_eps( "std-dev of 5000 10s", spc.stats_std_dev(spc_state), 0, 0.001)



function test_statistics(desc, test_data)
   local spc_state = spc.setup("Process Under Test","Units","Val")
   spc_state = spc.initialize_statistics(spc_state, 0, 0, 0, 30, 500)
   print("Running simulation test "..desc..".")
   for i,v in ipairs(test_data) do
      spc_state = spc.process_datum(spc_state, v.datum, i)
      if not eps_equal(spc.stats_avg(spc_state), v.avg, 0.001) or not eps_equal(spc.stats_std_dev(spc_state), v.std_dev, 0.002) then
	 print("  "..tostring(i).." avg: "..tostring(spc.stats_avg(spc_state)).." ("..tostring(v.avg)..")  std-dev: "..tostring(spc.stats_std_dev(spc_state)).." ("..tostring(v.std_dev)..").")
      end
   end
   return spc_state;
end


function test_detection(desc, test_data)
   local spc_state = spc.setup("Process Under Test","Units","Val")
   print("Running detection test "..desc..".")
   for i,v in ipairs(test_data) do
      spc_state = spc.process_datum(spc_state, v.datum, i)
      if (spc.check_for_anomalies(spc_state) ~= v.fault and v.fault) then
	 print("  FALSE NEGATIVE: "..tostring(i).." avg: "..tostring(spc.stats_avg(spc_state)).." ("..tostring(v.avg)..")  std-dev: "..tostring(spc.stats_std_dev(spc_state)).." ("..tostring(v.std_dev)..")  fault: "..tostring(spc.check_for_anomalies(spc_state)).." ("..tostring(v.fault)..").")
      elseif (spc.check_for_anomalies(spc_state) ~= v.fault) then
	 print("  FALSE POSITIVE: "..tostring(i).." avg: "..tostring(spc.stats_avg(spc_state)).." ("..tostring(v.avg)..")  std-dev: "..tostring(spc.stats_std_dev(spc_state)).." ("..tostring(v.std_dev)..")  fault: "..tostring(spc.check_for_anomalies(spc_state)).." ("..tostring(v.fault)..").")
      elseif (v.fault) then
	 print("  Correctly Detected: "..tostring(i).." avg: "..tostring(spc.stats_avg(spc_state)).." ("..tostring(v.avg)..")  std-dev: "..tostring(spc.stats_std_dev(spc_state)).." ("..tostring(v.std_dev)..")  fault: "..tostring(spc.check_for_anomalies(spc_state)).." ("..tostring(v.fault)..").")
      end
   end
   return spc_state;
end



normal_test_data =
   {{datum=9.757316, avg=9.757316, std_dev=0.000000, fault=false},
 {datum=8.518872, avg=9.138094, std_dev=0.875712, fault=false},
 {datum=5.656994, avg=7.977727, std_dev=2.103043, fault=false},
 {datum=7.114804, avg=7.761996, std_dev=1.770504, fault=false},
 {datum=8.360234, avg=7.881644, std_dev=1.556468, fault=false},
 {datum=8.960814, avg=8.061506, std_dev=1.460197, fault=false},
 {datum=14.689945, avg=9.008426, std_dev=2.837854, fault=false},
 {datum=10.305001, avg=9.170498, std_dev=2.667033, fault=false},
 {datum=7.781385, avg=9.016152, std_dev=2.537387, fault=false},
 {datum=8.668860, avg=8.981422, std_dev=2.394791, fault=false},
 {datum=8.060894, avg=8.897738, std_dev=2.288789, fault=false},
 {datum=10.362180, avg=9.019775, std_dev=2.222845, fault=false},
 {datum=9.867261, avg=9.084966, std_dev=2.141153, fault=false},
 {datum=12.385014, avg=9.320684, std_dev=2.238249, fault=false},
 {datum=8.720490, avg=9.280671, std_dev=2.162391, fault=false},
 {datum=8.300868, avg=9.219433, std_dev=2.103380, fault=false},
 {datum=7.984929, avg=9.146815, std_dev=2.058480, fault=false},
 {datum=9.627997, avg=9.173548, std_dev=2.000237, fault=false},
 {datum=7.881025, avg=9.105520, std_dev=1.966367, fault=false},
 {datum=8.626734, avg=9.081581, std_dev=1.916913, fault=false},
 {datum=12.412799, avg=9.240210, std_dev=2.004809, fault=false},
 {datum=4.247824, avg=9.013284, std_dev=2.227279, fault=false},
 {datum=8.862759, avg=9.006739, std_dev=2.176296, fault=false},
 {datum=10.994030, avg=9.089543, std_dev=2.166771, fault=false},
 {datum=10.922221, avg=9.162850, std_dev=2.152586, fault=false},
 {datum=3.634674, avg=8.950228, std_dev=2.371433, fault=false},
 {datum=11.213499, avg=9.034053, std_dev=2.365822, fault=false},
 {datum=8.162479, avg=9.002925, std_dev=2.327433, fault=false},
 {datum=11.481356, avg=9.088388, std_dev=2.331372, fault=false},
 {datum=11.391621, avg=9.165163, std_dev=2.329099, fault=false},
 {datum=9.163182, avg=9.165099, std_dev=2.289952, fault=false},
 {datum=11.965636, avg=9.252616, std_dev=2.306472, fault=false},
 {datum=13.156570, avg=9.370917, std_dev=2.369687, fault=false},
 {datum=4.715150, avg=9.233983, std_dev=2.466330, fault=false},
 {datum=8.722005, avg=9.219355, std_dev=2.431331, fault=false},
 {datum=6.976955, avg=9.157066, std_dev=2.425314, fault=false},
 {datum=7.953469, avg=9.124536, std_dev=2.399564, fault=false},
 {datum=2.847030, avg=8.959339, std_dev=2.576687, fault=false},
 {datum=16.110606, avg=9.142705, std_dev=2.788530, fault=false},
 {datum=3.028441, avg=8.989848, std_dev=2.917383, fault=true},
 {datum=6.426731, avg=8.927333, std_dev=2.908363, fault=false},
 {datum=10.329763, avg=8.960724, std_dev=2.880816, fault=false},
 {datum=12.942396, avg=9.053321, std_dev=2.910359, fault=false},
 {datum=7.885811, avg=9.026787, std_dev=2.881699, fault=false},
 {datum=8.719698, avg=9.019963, std_dev=2.849132, fault=false},
 {datum=4.328535, avg=8.917975, std_dev=2.900971, fault=false},
 {datum=17.228615, avg=9.094797, std_dev=3.114833, fault=false},
 {datum=5.277120, avg=9.015262, std_dev=3.130399, fault=false},
 {datum=13.686238, avg=9.110588, std_dev=3.168676, fault=false},
 {datum=8.436685, avg=9.097110, std_dev=3.137624, fault=false},
 {datum=14.304102, avg=9.199208, std_dev=3.190519, fault=false},
 {datum=5.094128, avg=9.120264, std_dev=3.209967, fault=false},
 {datum=9.637594, avg=9.130025, std_dev=3.179746, fault=false},
 {datum=8.726114, avg=9.122545, std_dev=3.150085, fault=false},
 {datum=10.926986, avg=9.155353, std_dev=3.130252, fault=true},
 {datum=11.151455, avg=9.190998, std_dev=3.113113, fault=false},
 {datum=8.746312, avg=9.183197, std_dev=3.085754, fault=false},
 {datum=11.622204, avg=9.225248, std_dev=3.075288, fault=false},
 {datum=10.559975, avg=9.247871, std_dev=3.053609, fault=false},
 {datum=12.736567, avg=9.306016, std_dev=3.060937, fault=false},
 {datum=7.415925, avg=9.275031, std_dev=3.044954, fault=false},
 {datum=8.937657, avg=9.269589, std_dev=3.020196, fault=false},
 {datum=11.943330, avg=9.312030, std_dev=3.014621, fault=false},
 {datum=7.148376, avg=9.278222, std_dev=3.002804, fault=false},
 {datum=12.839537, avg=9.333012, std_dev=3.011821, fault=false},
 {datum=3.402231, avg=9.243152, std_dev=3.076435, fault=false},
 {datum=6.396904, avg=9.200670, std_dev=3.072778, fault=false},
 {datum=12.328693, avg=9.246671, std_dev=3.073261, fault=false},
 {datum=11.994780, avg=9.286498, std_dev=3.068466, fault=false},
 {datum=9.981875, avg=9.296432, std_dev=3.047283, fault=false},
 {datum=5.378254, avg=9.241247, std_dev=3.060965, fault=false},
 {datum=10.571847, avg=9.259727, std_dev=3.043375, fault=false},
 {datum=10.871750, avg=9.281810, std_dev=3.028050, fault=false},
 {datum=5.283020, avg=9.227772, std_dev=3.042954, fault=false},
 {datum=9.707926, avg=9.234174, std_dev=3.022832, fault=false},
 {datum=7.675810, avg=9.213669, std_dev=3.007929, fault=false},
 {datum=9.712643, avg=9.220149, std_dev=2.988615, fault=false},
 {datum=11.184782, avg=9.245337, std_dev=2.977467, fault=false},
 {datum=7.682299, avg=9.225552, std_dev=2.963541, fault=false},
 {datum=7.699771, avg=9.206480, std_dev=2.949662, fault=false},
 {datum=6.048269, avg=9.167489, std_dev=2.952099, fault=false},
 {datum=10.737295, avg=9.186633, std_dev=2.938937, fault=false},
 {datum=7.504323, avg=9.166364, std_dev=2.926792, fault=false},
 {datum=9.256774, avg=9.167441, std_dev=2.909124, fault=false},
 {datum=13.580666, avg=9.219361, std_dev=2.931107, fault=false},
 {datum=12.339407, avg=9.255641, std_dev=2.933174, fault=false},
 {datum=9.061310, avg=9.253407, std_dev=2.916145, fault=false},
 {datum=11.090724, avg=9.274286, std_dev=2.905945, fault=false},
 {datum=7.276671, avg=9.251840, std_dev=2.897135, fault=false},
 {datum=8.813222, avg=9.246967, std_dev=2.881184, fault=false},
 {datum=5.146465, avg=9.201906, std_dev=2.897198, fault=false},
 {datum=9.226022, avg=9.202169, std_dev=2.881237, fault=false},
 {datum=7.087041, avg=9.179425, std_dev=2.873916, fault=false},
 {datum=6.686901, avg=9.152909, std_dev=2.869961, fault=false},
 {datum=15.133399, avg=9.215862, std_dev=2.919853, fault=false},
 {datum=10.298415, avg=9.227138, std_dev=2.906545, fault=false},
 {datum=12.027097, avg=9.256004, std_dev=2.905310, fault=false},
 {datum=13.737247, avg=9.301731, std_dev=2.925529, fault=false},
 {datum=8.514906, avg=9.293783, std_dev=2.911639, fault=false},
 {datum=16.216440, avg=9.363010, std_dev=2.978463, fault=false},
 {datum=7.604175, avg=9.345595, std_dev=2.968696, fault=false},
 {datum=5.806142, avg=9.310895, std_dev=2.974680, fault=false},
 {datum=12.745167, avg=9.344237, std_dev=2.979341, fault=false},
 {datum=11.406121, avg=9.364063, std_dev=2.971729, fault=false},
 {datum=13.287417, avg=9.401428, std_dev=2.982089, fault=false},
 {datum=9.961711, avg=9.406714, std_dev=2.968354, fault=false},
 {datum=6.484731, avg=9.379406, std_dev=2.967793, fault=false},
 {datum=13.588980, avg=9.418383, std_dev=2.981536, fault=false},
 {datum=11.347327, avg=9.436080, std_dev=2.973446, fault=false},
 {datum=14.002658, avg=9.477594, std_dev=2.991629, fault=false},
 {datum=12.089243, avg=9.501123, std_dev=2.988299, fault=false},
 {datum=9.721176, avg=9.503088, std_dev=2.974881, fault=false},
 {datum=13.421051, avg=9.537760, std_dev=2.984417, fault=false},
 {datum=12.796661, avg=9.566347, std_dev=2.986818, fault=false},
 {datum=13.007536, avg=9.596270, std_dev=2.990953, fault=false},
 {datum=8.030553, avg=9.582772, std_dev=2.981467, fault=false},
 {datum=13.354589, avg=9.615010, std_dev=2.988998, fault=true},
 {datum=11.278079, avg=9.629104, std_dev=2.980132, fault=false},
 {datum=8.179629, avg=9.616924, std_dev=2.970451, fault=false},
 {datum=7.160132, avg=9.596450, std_dev=2.966434, fault=false},
 {datum=11.202894, avg=9.609727, std_dev=2.957655, fault=false},
 {datum=9.552033, avg=9.609254, std_dev=2.945413, fault=false},
 {datum=10.809532, avg=9.619012, std_dev=2.935313, fault=false},
 {datum=14.217503, avg=9.656097, std_dev=2.952379, fault=false},
 {datum=11.931072, avg=9.674297, std_dev=2.947483, fault=false},
 {datum=7.173435, avg=9.654448, std_dev=2.944111, fault=false},
 {datum=8.282885, avg=9.643649, std_dev=2.934929, fault=false},
 {datum=13.189131, avg=9.671348, std_dev=2.940101, fault=false},
 {datum=14.811574, avg=9.711195, std_dev=2.963356, fault=false},
 {datum=9.615446, avg=9.710458, std_dev=2.951860, fault=false},
 {datum=13.669385, avg=9.740679, std_dev=2.960759, fault=false},
 {datum=11.769946, avg=9.756052, std_dev=2.954720, fault=false},
 {datum=11.013723, avg=9.765508, std_dev=2.945526, fault=false},
 {datum=15.158154, avg=9.805752, std_dev=2.971180, fault=false},
 {datum=8.889133, avg=9.798962, std_dev=2.961124, fault=false},
 {datum=8.427503, avg=9.788878, std_dev=2.952479, fault=false},
 {datum=14.439812, avg=9.822826, std_dev=2.968321, fault=false},
 {datum=5.902657, avg=9.794419, std_dev=2.976235, fault=false},
 {datum=9.955202, avg=9.795576, std_dev=2.965464, fault=false},
 {datum=9.296868, avg=9.792014, std_dev=2.955078, fault=false},
 {datum=7.570616, avg=9.776259, std_dev=2.950442, fault=false},
 {datum=11.694466, avg=9.789768, std_dev=2.944364, fault=false},
 {datum=4.018606, avg=9.749410, std_dev=2.973406, fault=false},
 {datum=13.097484, avg=9.772660, std_dev=2.976098, fault=false},
 {datum=13.440291, avg=9.797954, std_dev=2.981345, fault=false},
 {datum=9.219905, avg=9.793995, std_dev=2.971432, fault=false},
 {datum=9.789031, avg=9.793961, std_dev=2.961239, fault=false},
 {datum=11.209618, avg=9.803527, std_dev=2.953443, fault=false},
 {datum=7.326810, avg=9.786904, std_dev=2.950433, fault=false},
 {datum=10.677628, avg=9.792842, std_dev=2.941415, fault=false},
 {datum=10.494508, avg=9.797489, std_dev=2.932150, fault=false},
 {datum=12.076972, avg=9.812486, std_dev=2.928267, fault=false},
 {datum=8.381128, avg=9.803131, std_dev=2.920912, fault=false},
 {datum=8.709533, avg=9.796029, std_dev=2.912684, fault=false},
 {datum=9.700500, avg=9.795413, std_dev=2.903222, fault=false},
 {datum=12.670656, avg=9.813844, std_dev=2.902984, fault=false},
 {datum=10.484507, avg=9.818116, std_dev=2.894159, fault=false},
 {datum=10.727672, avg=9.823872, std_dev=2.885835, fault=false},
 {datum=12.153527, avg=9.838524, std_dev=2.882615, fault=false},
 {datum=10.423541, avg=9.842181, std_dev=2.873908, fault=true},
 {datum=9.983460, avg=9.843058, std_dev=2.864934, fault=true},
 {datum=9.980676, avg=9.843908, std_dev=2.856044, fault=true},
 {datum=11.802084, avg=9.855921, std_dev=2.851343, fault=true},
 {datum=9.940262, avg=9.856435, std_dev=2.842591, fault=true},
 {datum=8.424852, avg=9.847759, std_dev=2.836102, fault=true},
 {datum=10.143725, avg=9.849542, std_dev=2.827588, fault=true},
 {datum=8.933974, avg=9.844060, std_dev=2.819948, fault=false},
 {datum=7.665490, avg=9.831092, std_dev=2.816512, fault=false},
 {datum=11.222436, avg=9.839325, std_dev=2.810156, fault=false},
 {datum=15.537078, avg=9.872841, std_dev=2.835704, fault=false},
 {datum=7.179369, avg=9.857090, std_dev=2.834844, fault=false},
 {datum=10.285479, avg=9.859580, std_dev=2.826732, fault=false},
 {datum=10.668788, avg=9.864258, std_dev=2.819174, fault=false},
 {datum=9.908428, avg=9.864512, std_dev=2.811016, fault=false},
 {datum=11.758738, avg=9.875336, std_dev=2.806582, fault=false},
 {datum=9.088281, avg=9.870864, std_dev=2.799180, fault=false},
 {datum=11.652702, avg=9.880931, std_dev=2.794428, fault=false},
 {datum=8.247744, avg=9.871756, std_dev=2.789211, fault=false},
 {datum=14.807072, avg=9.899327, std_dev=2.805720, fault=false},
 {datum=13.882312, avg=9.921455, std_dev=2.813578, fault=false},
 {datum=6.500026, avg=9.902552, std_dev=2.817253, fault=false},
 {datum=10.371155, avg=9.905127, std_dev=2.809675, fault=false},
 {datum=9.198953, avg=9.901268, std_dev=2.802432, fault=false},
 {datum=4.711505, avg=9.873063, std_dev=2.820831, fault=false},
 {datum=11.367004, avg=9.881138, std_dev=2.815298, fault=false},
 {datum=8.844616, avg=9.875565, std_dev=2.808708, fault=false},
 {datum=13.039223, avg=9.892483, std_dev=2.810685, fault=false},
 {datum=11.497381, avg=9.901020, std_dev=2.805602, fault=false},
 {datum=13.232489, avg=9.918647, std_dev=2.808604, fault=false},
 {datum=8.157973, avg=9.909380, std_dev=2.804075, fault=false},
 {datum=15.853938, avg=9.940503, std_dev=2.829570, fault=false},
 {datum=10.053710, avg=9.941093, std_dev=2.822165, fault=false},
 {datum=9.668300, avg=9.939679, std_dev=2.814874, fault=false},
 {datum=10.814637, avg=9.944190, std_dev=2.808275, fault=false},
 {datum=13.625356, avg=9.963067, std_dev=2.813405, fault=false},
 {datum=4.885685, avg=9.937162, std_dev=2.829521, fault=false},
 {datum=11.555503, avg=9.945377, std_dev=2.824648, fault=false},
 {datum=11.287762, avg=9.952157, std_dev=2.819084, fault=false},
 {datum=12.703156, avg=9.965981, std_dev=2.818710, fault=false},
 {datum=12.290539, avg=9.977604, std_dev=2.816420, fault=false},
 {datum=10.364343, avg=9.979528, std_dev=2.809502, fault=false},
 {datum=9.802009, avg=9.978649, std_dev=2.802533, fault=false},
 {datum=10.634684, avg=9.981881, std_dev=2.795966, fault=false},
 {datum=11.364052, avg=9.988656, std_dev=2.790749, fault=false},
 {datum=8.126415, avg=9.979572, std_dev=2.786938, fault=false},
 {datum=6.980562, avg=9.965014, std_dev=2.787973, fault=false},
 {datum=10.746645, avg=9.968790, std_dev=2.781729, fault=false},
 {datum=5.632093, avg=9.947940, std_dev=2.791245, fault=false},
 {datum=13.796537, avg=9.966355, std_dev=2.797224, fault=false},
 {datum=13.676139, avg=9.984020, std_dev=2.802242, fault=false},
 {datum=8.172445, avg=9.975435, std_dev=2.798343, fault=false},
 {datum=13.543969, avg=9.992267, std_dev=2.802441, fault=false},
 {datum=6.305596, avg=9.974959, std_dev=2.807212, fault=false},
 {datum=3.890477, avg=9.946527, std_dev=2.831332, fault=false},
 {datum=8.629043, avg=9.940399, std_dev=2.826137, fault=false},
 {datum=9.606890, avg=9.938855, std_dev=2.819649, fault=false},
 {datum=13.446868, avg=9.955021, std_dev=2.823176, fault=false},
 {datum=15.624253, avg=9.981027, std_dev=2.842714, fault=false},
 {datum=11.349547, avg=9.987276, std_dev=2.837694, fault=false},
 {datum=7.405966, avg=9.975542, std_dev=2.836551, fault=false},
 {datum=10.146026, avg=9.976314, std_dev=2.830121, fault=false},
 {datum=11.702390, avg=9.984089, std_dev=2.826086, fault=false},
 {datum=10.954120, avg=9.988439, std_dev=2.820462, fault=false},
 {datum=9.497387, avg=9.986247, std_dev=2.814322, fault=false},
 {datum=7.865465, avg=9.976821, std_dev=2.811590, fault=false},
 {datum=10.228743, avg=9.977936, std_dev=2.805385, fault=false},
 {datum=10.872886, avg=9.981878, std_dev=2.799802, fault=false},
 {datum=10.277149, avg=9.983173, std_dev=2.793696, fault=false},
 {datum=10.852102, avg=9.986968, std_dev=2.788155, fault=false},
 {datum=11.290626, avg=9.992636, std_dev=2.783388, fault=false},
 {datum=12.281325, avg=10.002543, std_dev=2.781410, fault=false},
 {datum=12.812933, avg=10.014657, std_dev=2.781509, fault=false},
 {datum=10.467236, avg=10.016600, std_dev=2.775667, fault=false},
 {datum=12.883555, avg=10.028852, std_dev=2.776038, fault=true},
 {datum=8.568770, avg=10.022638, std_dev=2.771737, fault=false},
 {datum=10.253445, avg=10.023616, std_dev=2.765874, fault=false},
 {datum=7.983120, avg=10.015007, std_dev=2.763188, fault=false},
 {datum=11.788643, avg=10.022459, std_dev=2.759748, fault=false},
 {datum=10.984645, avg=10.026485, std_dev=2.754648, fault=false},
 {datum=14.812204, avg=10.046425, std_dev=2.766182, fault=false},
 {datum=5.123727, avg=10.025999, std_dev=2.778567, fault=false},
 {datum=11.549593, avg=10.032295, std_dev=2.774525, fault=false},
 {datum=10.949650, avg=10.036070, std_dev=2.769412, fault=false},
 {datum=9.743950, avg=10.034873, std_dev=2.763771, fault=false},
 {datum=10.930090, avg=10.038527, std_dev=2.758695, fault=false},
 {datum=11.892413, avg=10.046063, std_dev=2.755595, fault=false},
 {datum=11.903106, avg=10.053581, std_dev=2.752526, fault=false},
 {datum=9.702000, avg=10.052164, std_dev=2.747040, fault=false},
 {datum=11.367236, avg=10.057445, std_dev=2.742762, fault=false},
 {datum=12.533096, avg=10.067348, std_dev=2.741723, fault=false}}

test_statistics("Mean 10, std-dev 2.5, normal data", normal_test_data)
local spc_obj = test_detection("Mean 10, std-dev 2.5, normal data", normal_test_data);

function to_chart_json(spc_obj)
    local crt = spc.chart_description(spc_obj);
    local txt = '{"name":"'..crt.name..'","units":"'..crt.units..'","shortName":"'..crt.shortName..'","mean":'..crt.mean..',"dev":'..crt.dev..',"records":[';
    for i,rec in ipairs(crt.records) do
        txt = txt.. '{"value":'..rec.value..',"timestamp":'..rec.timestamp..',"faultCodes":[';
        for i2, fc in ipairs(rec.faultCodes) do
            txt = txt..'"'..fc..'",';
        end
        txt = txt.. ']},'
    end
    txt = txt.. ']}'
    return txt
end

function to_chart_js(spc_obj)
    local crt = spc.chart_description(spc_obj);
    local txt = '{name:"'..crt.name..'",units:"'..crt.units..'",shortName:"'..crt.shortName..'",mean:'..crt.mean..',dev:'..crt.dev..',records:[';
    for i,rec in ipairs(crt.records) do
        txt = txt.. '{value:'..rec.value..',timestamp:'..rec.timestamp..',faultCodes:[';
        for i2, fc in ipairs(rec.faultCodes) do
            txt = txt..'"'..fc..'",';
        end
        txt = txt.. ']},'
    end
    txt = txt.. ']}'
    return txt
end


-- local crt = spc.chart_description(spc_obj);
-- for i,rec in ipairs(crt.records) do
    -- for i2, v2 in ipairs(rec) do
        -- io.write(i2..':'..v2..',')
    -- end
    -- print()
-- end

print( to_chart_js(spc_obj) );


gaussian_bump_test_data =
   {{datum=0.388261, avg=0.388261, std_dev=0.000000, fault=false},
 {datum=-0.349217, avg=0.019522, std_dev=0.521476, fault=false},
 {datum=0.064332, avg=0.034459, std_dev=0.369646, fault=false},
 {datum=-0.141893, avg=-0.009629, std_dev=0.314431, fault=false},
 {datum=0.020725, avg=-0.003558, std_dev=0.272643, fault=false},
 {datum=-0.067094, avg=-0.014148, std_dev=0.245235, fault=false},
 {datum=0.072996, avg=-0.001699, std_dev=0.226278, fault=false},
 {datum=-0.069355, avg=-0.010156, std_dev=0.210854, fault=false},
 {datum=-0.174012, avg=-0.028362, std_dev=0.204659, fault=false},
 {datum=-0.428413, avg=-0.068367, std_dev=0.230728, fault=false},
 {datum=-0.197146, avg=-0.080074, std_dev=0.222305, fault=false},
 {datum=0.066955, avg=-0.067822, std_dev=0.216167, fault=false},
 {datum=0.177650, avg=-0.048939, std_dev=0.217875, fault=false},
 {datum=0.114899, avg=-0.037237, std_dev=0.213858, fault=false},
 {datum=-0.031012, avg=-0.036822, std_dev=0.206085, fault=false},
 {datum=0.091842, avg=-0.028780, std_dev=0.201679, fault=false},
 {datum=0.265112, avg=-0.011492, std_dev=0.207877, fault=false},
 {datum=-0.220756, avg=-0.023118, std_dev=0.207614, fault=false},
 {datum=0.298773, avg=-0.006177, std_dev=0.214854, fault=false},
 {datum=0.091354, avg=-0.001300, std_dev=0.210258, fault=false},
 {datum=0.087937, avg=0.002949, std_dev=0.205857, fault=false},
 {datum=0.005457, avg=0.003063, std_dev=0.200897, fault=false},
 {datum=0.060490, avg=0.005560, std_dev=0.196643, fault=false},
 {datum=-0.192105, avg=-0.002676, std_dev=0.196507, fault=false},
 {datum=-0.220744, avg=-0.011399, std_dev=0.197252, fault=false},
 {datum=-0.159444, avg=-0.017093, std_dev=0.195435, fault=false},
 {datum=-0.363606, avg=-0.029926, std_dev=0.202911, fault=false},
 {datum=-0.225902, avg=-0.036926, std_dev=0.202533, fault=false},
 {datum=0.306540, avg=-0.025082, std_dev=0.208860, fault=false},
 {datum=-0.235022, avg=-0.032080, std_dev=0.208776, fault=false},
 {datum=0.086294, avg=-0.028261, std_dev=0.206365, fault=false},
 {datum=-0.095862, avg=-0.030374, std_dev=0.203361, fault=false},
 {datum=0.058171, avg=-0.027691, std_dev=0.200751, fault=false},
 {datum=0.202069, avg=-0.020933, std_dev=0.201575, fault=false},
 {datum=-0.071267, avg=-0.022371, std_dev=0.198770, fault=false},
 {datum=0.138849, avg=-0.017893, std_dev=0.197744, fault=false},
 {datum=0.071951, avg=-0.015465, std_dev=0.195537, fault=false},
 {datum=-0.327730, avg=-0.023682, std_dev=0.199418, fault=false},
 {datum=0.304934, avg=-0.015256, std_dev=0.203691, fault=false},
 {datum=-0.398795, avg=-0.024845, std_dev=0.210008, fault=false},
 {datum=-0.303672, avg=-0.031645, std_dev=0.211890, fault=false},
 {datum=0.006006, avg=-0.030749, std_dev=0.209370, fault=false},
 {datum=0.393858, avg=-0.020874, std_dev=0.216760, fault=false},
 {datum=0.124675, avg=-0.017566, std_dev=0.215346, fault=false},
 {datum=0.065738, avg=-0.015715, std_dev=0.213246, fault=false},
 {datum=-0.233847, avg=-0.020457, std_dev=0.213302, fault=false},
 {datum=-0.506066, avg=-0.030789, std_dev=0.222545, fault=false},
 {datum=-0.802081, avg=-0.046858, std_dev=0.246710, fault=true},
 {datum=-0.099194, avg=-0.047926, std_dev=0.244241, fault=false},
 {datum=0.011807, avg=-0.046731, std_dev=0.241884, fault=false},
 {datum=-0.233821, avg=-0.050400, std_dev=0.240882, fault=false},
 {datum=-0.068678, avg=-0.050751, std_dev=0.238522, fault=false},
 {datum=-0.416954, avg=-0.057661, std_dev=0.241514, fault=false},
 {datum=0.307354, avg=-0.050901, std_dev=0.244327, fault=false},
 {datum=-0.402910, avg=-0.057301, std_dev=0.246664, fault=false},
 {datum=0.331486, avg=-0.050359, std_dev=0.249872, fault=false},
 {datum=-0.183975, avg=-0.052703, std_dev=0.248263, fault=false},
 {datum=0.719613, avg=-0.039387, std_dev=0.266152, fault=false},
 {datum=-0.420103, avg=-0.045840, std_dev=0.268463, fault=false},
 {datum=0.079265, avg=-0.043755, std_dev=0.266668, fault=false},
 {datum=-0.067877, avg=-0.044150, std_dev=0.264454, fault=false},
 {datum=0.147736, avg=-0.041055, std_dev=0.263407, fault=true},
 {datum=-0.539427, avg=-0.048966, std_dev=0.268713, fault=true},
 {datum=-0.328381, avg=-0.053332, std_dev=0.268850, fault=true},
 {datum=0.334954, avg=-0.047358, std_dev=0.271055, fault=false},
 {datum=0.106337, avg=-0.045029, std_dev=0.269626, fault=false},
 {datum=0.330000, avg=-0.039432, std_dev=0.271470, fault=false},
 {datum=0.256325, avg=-0.035083, std_dev=0.271813, fault=false},
 {datum=0.214865, avg=-0.031460, std_dev=0.271480, fault=false},
 {datum=-0.012822, avg=-0.031194, std_dev=0.269514, fault=false},
 {datum=-0.322071, avg=-0.035291, std_dev=0.269800, fault=false},
 {datum=-0.220228, avg=-0.037859, std_dev=0.268778, fault=false},
 {datum=-0.228163, avg=-0.040466, std_dev=0.267833, fault=false},
 {datum=0.567982, avg=-0.032244, std_dev=0.275236, fault=false},
 {datum=-0.359412, avg=-0.036606, std_dev=0.275968, fault=false},
 {datum=0.150221, avg=-0.034148, std_dev=0.274958, fault=false},
 {datum=-0.209127, avg=-0.036420, std_dev=0.273870, fault=false},
 {datum=-0.470857, avg=-0.041990, std_dev=0.276497, fault=false},
 {datum=-0.119500, avg=-0.042971, std_dev=0.274857, fault=false},
 {datum=-0.035559, avg=-0.042879, std_dev=0.273113, fault=false},
 {datum=0.459219, avg=-0.036680, std_dev=0.277075, fault=false},
 {datum=0.042477, avg=-0.035715, std_dev=0.275499, fault=false},
 {datum=0.137940, avg=-0.033622, std_dev=0.274476, fault=false},
 {datum=-0.153310, avg=-0.035047, std_dev=0.273130, fault=false},
 {datum=-0.113255, avg=-0.035967, std_dev=0.271632, fault=false},
 {datum=0.447535, avg=-0.030345, std_dev=0.275017, fault=false},
 {datum=0.449378, avg=-0.024831, std_dev=0.278208, fault=false},
 {datum=0.465893, avg=-0.019255, std_dev=0.281508, fault=false},
 {datum=0.241587, avg=-0.016324, std_dev=0.281266, fault=false},
 {datum=0.116984, avg=-0.014843, std_dev=0.280034, fault=false},
 {datum=0.097360, avg=-0.013610, std_dev=0.278723, fault=false},
 {datum=0.605596, avg=-0.006879, std_dev=0.284605, fault=false},
 {datum=0.426846, avg=-0.002215, std_dev=0.286605, fault=false},
 {datum=0.697482, avg=0.005228, std_dev=0.294054, fault=true},
 {datum=0.612886, avg=0.011625, std_dev=0.299056, fault=true},
 {datum=1.215488, avg=0.024165, std_dev=0.321854, fault=true},
 {datum=0.973955, avg=0.033956, std_dev=0.334381, fault=true},
 {datum=0.572172, avg=0.039448, std_dev=0.337067, fault=true},
 {datum=0.838607, avg=0.047521, std_dev=0.344827, fault=true},
 {datum=1.526234, avg=0.062308, std_dev=0.373591, fault=true},
 {datum=1.281144, avg=0.074376, std_dev=0.391003, fault=true},
 {datum=0.758825, avg=0.081086, std_dev=0.394921, fault=true},
 {datum=0.834761, avg=0.088403, std_dev=0.399935, fault=true},
 {datum=0.716421, avg=0.094442, std_dev=0.402725, fault=true},
 {datum=0.651998, avg=0.099752, std_dev=0.404461, fault=true},
 {datum=0.772017, avg=0.106094, std_dev=0.407792, fault=true},
 {datum=0.457535, avg=0.109378, std_dev=0.407284, fault=true},
 {datum=0.746026, avg=0.115273, std_dev=0.409979, fault=true},
 {datum=0.176348, avg=0.115834, std_dev=0.408118, fault=true},
 {datum=0.483644, avg=0.119177, std_dev=0.407753, fault=true},
 {datum=0.666792, avg=0.124111, std_dev=0.409210, fault=true},
 {datum=0.448299, avg=0.127005, std_dev=0.408512, fault=true},
 {datum=0.363896, avg=0.129102, std_dev=0.407295, fault=true},
 {datum=-0.044310, avg=0.127581, std_dev=0.405814, fault=false},
 {datum=0.434502, avg=0.130249, std_dev=0.405042, fault=false},
 {datum=-0.122293, avg=0.128072, std_dev=0.403958, fault=false},
 {datum=0.184821, avg=0.128557, std_dev=0.402248, fault=false},
 {datum=-0.617457, avg=0.122235, std_dev=0.406370, fault=false},
 {datum=-0.162277, avg=0.119844, std_dev=0.405484, fault=false},
 {datum=0.578183, avg=0.123664, std_dev=0.405939, fault=false},
 {datum=0.157518, avg=0.123944, std_dev=0.404256, fault=false},
 {datum=0.122798, avg=0.123934, std_dev=0.402582, fault=false},
 {datum=-0.110415, avg=0.122029, std_dev=0.401485, fault=false},
 {datum=-0.512230, avg=0.116914, std_dev=0.403886, fault=false},
 {datum=-0.043226, avg=0.115633, std_dev=0.402509, fault=false},
 {datum=-0.582630, avg=0.110091, std_dev=0.405693, fault=false},
 {datum=0.099928, avg=0.110011, std_dev=0.404081, fault=false},
 {datum=0.130142, avg=0.110168, std_dev=0.402491, fault=false},
 {datum=0.029517, avg=0.109543, std_dev=0.400979, fault=false},
 {datum=0.062016, avg=0.109178, std_dev=0.399443, fault=false},
 {datum=-0.439088, avg=0.104992, std_dev=0.400777, fault=false},
 {datum=-0.393987, avg=0.101212, std_dev=0.401600, fault=false},
 {datum=-0.145032, avg=0.099361, std_dev=0.400645, fault=false},
 {datum=-0.126368, avg=0.097676, std_dev=0.399612, fault=false},
 {datum=-0.303341, avg=0.094706, std_dev=0.399611, fault=false},
 {datum=-0.224175, avg=0.092361, std_dev=0.399066, fault=false},
 {datum=0.211593, avg=0.093231, std_dev=0.397727, fault=false},
 {datum=0.161746, avg=0.093728, std_dev=0.396316, fault=false},
 {datum=0.299397, avg=0.095207, std_dev=0.395262, fault=false},
 {datum=0.243234, avg=0.096265, std_dev=0.394037, fault=false},
 {datum=-0.008930, avg=0.095519, std_dev=0.392727, fault=false},
 {datum=0.390371, avg=0.097595, std_dev=0.392113, fault=false},
 {datum=0.207786, avg=0.098366, std_dev=0.390839, fault=false},
 {datum=-0.118738, avg=0.096858, std_dev=0.389890, fault=false},
 {datum=-0.101779, avg=0.095488, std_dev=0.388883, fault=false},
 {datum=-0.342566, avg=0.092488, std_dev=0.389232, fault=false},
 {datum=0.125118, avg=0.092710, std_dev=0.387906, fault=false},
 {datum=-0.021528, avg=0.091938, std_dev=0.386699, fault=false},
 {datum=-0.303533, avg=0.089284, std_dev=0.386749, fault=false},
 {datum=0.040755, avg=0.088960, std_dev=0.385470, fault=false}}


test_statistics("Gaussian bump data", gaussian_bump_test_data)
test_detection("Gaussian bump data", gaussian_bump_test_data)

-- Testing for the statistics initialization
local state = spc.initialize_statistics(spc.setup("","",""), 3.4, 10, 1, 30, 100)
test_eps("Testing 1 point stats mean initializaton.", spc.stats_avg(state), 3.4, 0.001)
test_eps("Testing 1 point stats deviation initializaton.", spc.stats_std_dev(state), 0, 0.001)

local state = spc.initialize_statistics(spc.setup("","",""), 3.4, 10, 3, 30, 100)
test_eps("Testing 3 point stats mean initializaton.", spc.stats_avg(state), 3.4, 0.001)
test_eps("Testing 3 point stats deviation initializaton.", spc.stats_std_dev(state), 10, 0.001)

local state = spc.initialize_statistics(spc.setup("","",""), -3.4, 33, 30, 30, 100)
test_eps("Testing 30 point stats mean initializaton.", spc.stats_avg(state), -3.4, 0.001)
test_eps("Testing 30 point stats deviation initializaton.", spc.stats_std_dev(state), 33, 0.001)

-- os.execute( 'pause' )
