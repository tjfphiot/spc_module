spc = {}

-- To simplify computation, we'll normalize the elements in the
-- sequences to have zero mean and unit variance.  This converts from records
-- to normalized floats.
function spc.normalize_datum( record, mu, sigma )
   return ( (record.value - mu) / sigma )
end

function spc.normalize_sequence( xs, mu, sigma )
   local res = {}
   for i, x in ipairs( xs ) do
      res[i] = spc.normalize_datum( x, mu, sigma )
   end
   return res
end



-- Define simple predicates to detect which block the normalized
-- element is in.
function spc.outside_pos(r)
   return (function (x) return (r <= x) end)
end

function spc.outside_neg(r)
   return (function (x) return (x <= -r) end)
end

function spc.outside(r)
   return (function (x) return ((x<=-r) or (r<=x)) end)
end

function spc.inside(r)
   return (function (x) return ((-r<=x) and (x<=r)) end)
end

function spc.positive( x )
   return (0 <= x)
end

function spc.negative( x )
   return (x < 0)
end



-- Since many of the SPC rules are about counting the number of times
-- a predicate was satisfied over the past M samples, we build an
-- explicit helper function for that task.

function spc.satisfied_by_n_of_m( pred, n, m, xs )
   if (#xs < m) then
      return false
   else
      -- To improve specificity (and avoid multiply triggering in some cases),
      -- we require that the predicate be satisfied for the most recent element.
      if not pred(xs[1]) then
         return false;
      end
      local n_found = 0
      for i, x in ipairs(xs) do
	 if (i <= m and pred( x )) then
	    n_found = n_found + 1
	 end
      end
      return (n <= n_found)
   end
end


-- For change-based rules
function spc.test_pairwise( pred2, m, xs )
   if (#xs < 2) or (#xs < m) then
      return true
   else
      for i, v in ipairs(xs) do
	 if (m < i) then
	    return true
	 end
	 if (i ~= 1) then
	    if not pred2(v, xs[i-1]) then
	       return false
	    end
	 end
      end
      return true
   end
end

function spc.monotone_sequence( m, xs )
   return (spc.test_pairwise( function (a,b) return a <= b end, m, xs )
	      or spc.test_pairwise( function (a,b) return a >= b end, m, xs ))
end

function spc.alternating_sequence(m, xs)
   if (#xs < 2) or (#xs < m) then
      return false
   else
      if (xs[1] - xs[2] < 0) then
	 sign = 1
      else
	 sign = -1
      end
      for i,v in ipairs(xs) do
	 if (m < i) then
	    return true
	 elseif (i ~= 1) then
	    if (sign *(xs[i-1] - xs[i]) > 0) then
	       return false
	    else
	       sign = sign * -1
	    end
	 end
      end
      return true
   end
end



-- The rules themselves.  These assume you are passing in a normalized
-- sequence.
function spc.rule_1( xs )
   -- No point may exceed 3 standard deviations of the mean.
   if (#xs < 1) then
      return false;
   else
      return (spc.outside(3)(xs[1]))
   end
end

function spc.rule_2( xs )
   -- Nine points must not be on the same side of the mean.
   return (spc.satisfied_by_n_of_m( spc.positive, 9, 9, xs )
	      or spc.satisfied_by_n_of_m( spc.negative, 9, 9, xs))
end

function spc.rule_3( xs )
   -- Six or more points in a row, continuously increasing or
   -- decreasing.
   if (#xs < 6) then
      return false
   else
      return spc.monotone_sequence( 6, xs )
   end
end


function spc.rule_4( xs )
   -- TODO: 14 points alternating in slope...
   return spc.alternating_sequence(14, xs)
end

function spc.rule_5( xs )
   -- 2 out of 3 points 2 std deviations out, on the same side.
   return (spc.satisfied_by_n_of_m( spc.outside_pos(2), 2, 3, xs )
	      or spc.satisfied_by_n_of_m( spc.outside_neg(2), 2, 3, xs ))
end

function spc.rule_6( xs )
   -- 4 of 5 more than one std deviation out, on the same side.
   return (spc.satisfied_by_n_of_m( spc.outside_pos(1), 4, 5, xs )
	      or spc.satisfied_by_n_of_m( spc.outside_neg(1), 4, 5, xs ))
end

function spc.rule_7( xs )
   -- 15 points within 1 standard deviation of the mean.
   return spc.satisfied_by_n_of_m( spc.inside(1), 15, 15, xs )
end

function spc.rule_8( xs )
   -- 8 points outside of the 1 standard deviation region.
   return spc.satisfied_by_n_of_m( spc.outside(1), 8, 8, xs )
end
   

local spc_rule_set = 
    {{rule=spc.rule_1, fault_text="Extreme variance: Measurement outside 3&sigma;."},
     {rule=spc.rule_2, fault_text="Bias: 9 measurements on same side of mean."},
     {rule=spc.rule_3, fault_text="Trend: 6 increasing (or decreasing) measurements in a row."},
     {rule=spc.rule_4, fault_text="Anti-correlation: 15 measurements of alternating direction."},
     {rule=spc.rule_5, fault_text="High variance: 2 out of 3 measurements outside 2&sigma;."},
     {rule=spc.rule_6, fault_text="High variance: 4 out of 5 measurements outside 1&sigma;."},
     {rule=spc.rule_7, fault_text="Low deviation: 15 measurements inside of &plusmn;1&sigma;"},
     {rule=spc.rule_8, fault_text="Bi-modality: 8 measurements outside of &plusmn;1&sigma;"}}
   

-- The main business function of the module.
function spc.test_seq( seq, mu, sigma )
    return #spc.detect_faults(seq, mu, sigma) ~= 0;
end

-- Compile a list of fault codes that apply to the sequence.
function spc.detect_faults(seq, mu, sigma)
    local xs = spc.normalize_sequence( seq, mu, sigma )
    local fault_codes = {}
    for i, v in ipairs(spc_rule_set) do
        if v.rule(xs) then
            fault_codes[#fault_codes+1] = v.fault_text;
        end
    end
    return fault_codes;
end


-- Validate the SPC state object.  Call this first thing in all API functions.
-- It raises an exception if the state is invalid.
function spc.assert_valid_spc_state(spc_state)
    if not spc.validate_spc_state(spc_state) then
        error('Invalid SPC object.')
    end
end

local every_element = function(predicate, array)
    for i,v in ipairs(array) do
        if not predicate(v) then
            return false
        end
    end
    return true
end

function spc.validate_spc_state(spc_state)
    local validate_memory = function (memory)
        return (memory and
                every_element(function (x) return (x.value and x.timestamp and x.fault_codes); end, memory));
        end
    local validate_statistics = function (stats)
        return (stats.count and
                stats.count >= 0 and
                stats.avg and
                stats.var_a and
                stats.var_b and
                stats.var_c and
                stats.horizon and
                stats.horizon > 1 and
                stats.cut_in and
                stats.cut_in > 1);
        end
    return (spc_state.name and
            spc_state.units and
            spc_state.short_name and
            spc_state.memory and
            validate_memory(spc_state.memory) and
            spc_state.stats and
            validate_statistics(spc_state.stats));
end


-- Essentially a push operation.
function spc.update_memory(mem, datum, timestamp)
   local memory_size = 30
   local new_mem = {{value=datum, timestamp=timestamp, fault_codes={}}}
   for i,v in ipairs(mem) do
      if i < memory_size then
         new_mem[i+1] = v
      end
   end
   return new_mem
end

function spc.add_fault_codes_to_last_record(spc_obj, fault_codes)
    if #spc_obj.memory ~= 0 then
        spc_obj.memory[1].fault_codes = fault_codes;
    end
    return spc_obj;
end


-- The statistics tracking module is pretty hardcore Frankesoft--it's
-- based on a handful of clever math tricks that totally make sense to
-- me, but the derivation is a good page or two so it's not explained
-- here.  Rely on the tests to convince yourself of its correctness.

function spc.update_statistics(stats, datum)   
   local new_stats = {horizon = stats.horizon, cut_in = stats.cut_in}
   local n = stats.count
   new_stats.avg = (n*stats.avg + datum) / (n+1)
   if n == 0 then
      -- Note, these are not correct variance estimates, I'm holding
      -- these values here so they don't get lost... essentially, this
      -- lets me get around dividing by zero, and multiplying by it.
      -- I make use of these numbers in the n==1 case.  Be sure that
      -- cut_in is always greater than 2!!!  I'll hard code that.
      new_stats.var_a = datum*datum
      new_stats.var_b = datum
      new_stats.var_c = 1.0
   elseif n == 1 then
      new_stats.var_a = datum*datum + stats.var_a
      new_stats.var_b = datum + stats.var_b
      new_stats.var_c = 2.0
   else
      new_stats.var_a = (datum*datum + (n-1)*stats.var_a) / n
      new_stats.var_b = (datum + (n-1)*stats.var_b) / n
      new_stats.var_c = (1.0 + (n-1)*stats.var_c) / n
   end
   if n < stats.horizon then
      new_stats.count = n + 1
   else
      new_stats.count = n
   end
   return new_stats
end



-- This is the external API



-- Process a reading.  Call this first, then look for anomalies or
-- statistics.  Returns an updated state object.  This may be called
-- with a NIL spc_obj.
function spc.process_datum(spc_obj, datum, timestamp)
   spc.assert_valid_spc_state(spc_obj);
   if (type(datum) ~= "number") then
      return spc_obj
   end
   local new_spc_obj = {name=spc_obj.name, units=spc_obj.units, short_name=spc_obj.short_name}
   new_spc_obj.memory = spc.update_memory(spc_obj.memory, datum, timestamp)
   new_spc_obj.stats = spc.update_statistics(spc_obj.stats, datum)
   local avg = spc.stats_avg(new_spc_obj)
   local std = spc.stats_std_dev(new_spc_obj)
   local fault_codes = {}
   if (new_spc_obj.stats.cut_in <= new_spc_obj.stats.count) then
      fault_codes = spc.detect_faults(new_spc_obj.memory, avg, std);
   end
   return spc.add_fault_codes_to_last_record(new_spc_obj, fault_codes);
end

-- Returns the control chart specification object needed by the web application.
function spc.chart_description(spc_obj)
   spc.assert_valid_spc_state(spc_obj);
   local records = {}
   for i,rec in ipairs(spc_obj.memory) do
      records[#records+1] = {value=rec.value, timestamp=rec.timestamp, faultCodes=rec.fault_codes};
   end
   local chart_desc = {}
   chart_desc.name = spc_obj.name;
   chart_desc.units = spc_obj.units;
   chart_desc.shortName = spc_obj.short_name;
   chart_desc.records = spc_obj.memory;
   chart_desc.mean = spc.stats_avg(spc_obj);
   chart_desc.dev = spc.stats_std_dev(spc_obj);
   chart_desc.records = records;
   return chart_desc;
end

-- Return the average value of the stream.
function spc.stats_avg(spc_obj)
   spc.assert_valid_spc_state(spc_obj);
   local stats = spc_obj.stats
   if (spc_obj.stats
	  and spc_obj.stats.count
	  and (1 <= stats.count)
      and stats.avg) then
      return stats.avg
   else
      return 0.0
   end
end

-- Returns the standard deviation of the data stream.
function spc.stats_std_dev(spc_obj)
   spc.assert_valid_spc_state(spc_obj);
   local stats = spc_obj.stats
   if (2 <= stats.count) then
      return math.sqrt(math.abs(stats.var_a - 2*stats.avg*stats.var_b + stats.avg*stats.avg*stats.var_c))
   else
      return 0.0
   end
end

-- Called after processing the most recent datum.  Returns true if any
-- SPC rules are violated.
function spc.check_for_anomalies(spc_obj)
   spc.assert_valid_spc_state(spc_obj);
   if #spc_obj.memory == 0 then
      return false
   else
      return (#spc_obj.memory[1].fault_codes ~= 0);
   end
end


-- For initializing the statistics system.
function spc.initialize_statistics(spc_obj, mean, deviation, effective_count, cut_in, horizon)
    if (1 < effective_count) then
        local nn1 = effective_count / (effective_count - 1);
        spc_obj.stats = {count=effective_count, avg=mean, var_a=deviation*deviation+nn1*(mean*mean), var_b=nn1*mean, var_c=nn1, cut_in=cut_in, horizon=horizon};
        return spc_obj;
    elseif (effective_count == 1) then
        spc_obj.stats = {count=1, avg=mean, var_a=mean*mean, var_b=mean, var_c=1, cut_in=cut_in, horizon=horizon};
        return spc_obj;
    else
        spc_obj.stats = {count=0, avg=0, var_a=0, var_b=0, var_c=0, cut_in=cut_in, horizon=horizon};
        return spc_obj;
    end
end 


-- For initializing the state of the problem.
function spc.setup(name, units, short_name)
    local state = {name=name, units=units, short_name=short_name, stats={}, memory={}};
    return spc.initialize_statistics(state, 0, 0, 0, 30, 100);
end

-- Overview of usage:

-- For each SPC signal...

-- 1) Load SPC_state_string from keystore DB, JSON convert to
-- SPC_state (a Lua object).  Note, when starting, obtain an initial object
-- by calling `spc_state = spc.setup( name, units, short_name )`.  Optionally, 
-- setup particular statistics settings with 
-- `spc_state = spc.initialize_statistics(spc_state, avg, dev, count, cut_in, horizon)`.  By
-- default, the setup method sets reasonable defaults for the statistics, so 
-- don't call that method unless you know what you're doing.

-- 2) Call new_spc_state = spc.process_datum(spc_state, latest_value, 
-- timestamp), it returns an updated SPC_state.  Timestamp is expected to be a 
-- Unix timestamp in seconds.

-- 3) Optional:  Call spc.check_for_anomalies(new_spc_state).  If it returns
-- True, signal an alert.  Note that until the 'cut_in' many samples
-- have been processed, this logic will not trigger.  This helps
-- ensure we have good statistics estimates before using them to
-- enforce consistency.

-- 4) Optional:  Call `spc.stats_avg(new_spc_state)` and
-- `spc.stats_std_dev(new_spc__state)` to get the average and standard
-- deviation.  This is needed for drawing the control plot.

-- 5) Optional:  Call `spc.chart_description(new_spc_state)` to get the Lua
-- representation of the chart object.  Convert this to JSON, and pass it to 
-- the control chart front-end javascript.  Likely, you'll want to store this
-- string in the keystore database.

-- 6) JSON convert new_SPC_state to string, store in your DB for next
-- time.




return spc

