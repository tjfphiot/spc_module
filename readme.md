# SPC Module for Murano

Provides Statistical Process Control (SPC) functionality for Murano 
applications.  This module implements Nelson's Rules for SPC and can detect a 
wide range of process variance.  Further, the mean and standard deviation are 
automatically detected on-line, allowing this to work out-of-the-box without
any pre-work on signal analysis.

# Usage

Include the file `spc_module.lua` in your Murano scripts directory.

For each SPC signal...

1. Load spc_state_string from keystore DB, JSON convert to
spc_state (a Lua object).  Note, when starting, obtain an initial object
by calling `spc_state = spc.setup(name, units, short_name)`.  Optionally, 
setup particular statistics settings with 
`spc_state = spc.initialize_statistics(spc_state, avg, dev, count, cut_in, horizon)`.  By
default, the setup method sets reasonable defaults for the statistics, so 
don't call that method unless you know what you're doing.

2. Call `new_spc_state = spc.process_datum(spc_state, latest_value, 
timestamp)`, it returns an updated SPC_state.  Timestamp is expected to be a 
Unix timestamp in seconds.

3. Optional:  Call `spc.check_for_anomalies(new_spc_state)`.  If it returns
True, signal an alert.  Note that until the `cut_in` many samples
have been processed, this logic will not trigger.  This helps
ensure we have good statistics estimates before using them to
enforce consistency.

4. Optional:  Call `spc.stats_avg(new_spc_state)` and
`spc.stats_std_dev(new_spc_state)` to get the average and standard
deviation.  This is needed for drawing the control plot.

5. Optional:  Call `spc.chart_description(new_spc_state)` to get the Lua
representation of the chart object.  Convert this to JSON, and pass it to 
the control chart front-end javascript.  Likely, you'll want to store this
string in the keystore database.

6. JSON convert `new_spc_state` to string, store in your DB for next
time.


# Testing

The file `spc_module_test.lua` is meant for off-line testing of the module.  Do
not include it in your Murano project.  Running this file completes a set of 
automated tests that exercise a range of SPC test scenarios.
