## For generating test data.  Given a sequence of readings, this function 
## generates a string representation of the Lua datastructure used for testing.

function str = mk_test_vec(seq)

  avgs = mk_avgs(seq);
  std_devs = mk_std_devs(seq, avgs);

  ## Print the table
  str = '{';
  for n = 1:length(seq)
    if n == 1
      prefix = "";
    else
      prefix = ",\n ";
    end
    if n < 30
      fault_str = "false";
    else
      mem = (seq(n-29:n)-avgs(n))/std_devs(n);
      if apply_spc_rules(mem);
	fault_str = "true";
      else
	fault_str = "false";
      end
    end
    str = [str prefix sprintf('{datum=%f, avg=%f, std_dev=%f, fault=%s}', seq(n), avgs(n), std_devs(n), fault_str)];
  end
  str = [str  '}'];
end


function avgs = mk_avgs(seq)
  avgs = [];
  for n = 1:length(seq)
    a = sum(seq(1:n))/n;
    avgs = [avgs a];
  end
end

function std_devs = mk_std_devs(seq, avgs)
  std_devs = [0];
  for n = 2:length(seq)
    a = avgs(n);
    s = sqrt(sum((seq(1:n)-a).^2)/(n-1));
    std_devs = [std_devs s];
  end
end


## Transform to have zero mean and unit variance before calling this
## method.
function fault = apply_spc_rules(mem)
  N = length(mem);
  fault = false;
  
  ## Rule 1: One sample more than 3 away.
  if (mem(N) <= -3) || (3 <= mem(N))
    fault = true;
    printf('Rule 1')
  end

  ## Rule 2: Last 9 points on the same side of zero
  if all(mem(N-8:N) < 0) || all(0 < mem(N-8:N))
    fault = true;
    printf('Rule 2')
  end

  ## Rule 3: Last six points increasing or decreasing
  diffs = mem(N-4:N) - mem(N-5:N-1);
  if all(diffs < 0) || all(0 < diffs)
    fault = true;
    printf('Rule 3')
  end

  ## Rule 4: 14 points, alternating direction
  diffs = mem(N-12:N) - mem(N-13:N-1);
  diffs_alt = (-1).^(1:length(diffs)).*diffs;
  if all(diffs_alt < 0) || all(0 < diffs_alt)
    fault = true;
    printf('Rule 4')
  end

  ## Rule 5: 2 out of 3 points 2+ away on the same side
  if 2<=sum(2 <= mem(N-2:N)) || 2<=sum(mem(N-2:N) <= -2)
    fault = true;
    printf('Rule 5')
  end

  ## Rule 6: 4 out of 5 1+ away on the same side
  if 4<=sum(mem(N-4:N)<=-1) || 4<=sum(1<=mem(N-4:N))
    fault = true;
    printf('Rule 6')
  end

  ## Rule 7: 15 points within +/- 1
  if all(-1<mem(N-14:N)) && all(mem(N-14:N)<1)
    fault = true;
    printf('Rule 7')
  end

  ## Rule 8: 8 points all outside +/- 1
  if !(any(-1<mem(N-7:N) & mem(N-7:N)<1))
    fault = true;
    printf('Rule 8')
  end
  
end
